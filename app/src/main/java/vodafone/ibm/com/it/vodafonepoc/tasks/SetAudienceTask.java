/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSResponse;

import vodafone.ibm.com.it.vodafonepoc.Audience;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public class SetAudienceTask extends BasicTask
{
    private Audience audience;

    /**
     * @param session
     * @param audience   Audience configuration
     * @see Audience
     */
    public SetAudienceTask(Session session, Audience audience)
    {
        super(session);
        this.audience = audience;
    }

    @Override
    public String perform()
    {
        AXSResponse response = null;
        Log.d(Logic.TAG, "Starting Session on Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();

            response = proxy.setAudience(this.session.getSessionId(), this.audience);

            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            this.statusCode = response.statusCode;
        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
        }

        return InteractProxy.processResponse(response);
    }
}
