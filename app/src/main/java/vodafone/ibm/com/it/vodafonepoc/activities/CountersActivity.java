/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;

import java.util.ArrayList;

import vodafone.ibm.com.it.vodafonepoc.Counters;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.Session;
import vodafone.ibm.com.it.vodafonepoc.Settings;
import vodafone.ibm.com.it.vodafonepoc.tasks.PostEventTask;

public class CountersActivity extends MenuActivity
{
    private double indivId;
    private Counters counters;
    private Settings settings;

    private ProgressBar prgVoce;
    private ProgressBar prgDati;
    private ProgressBar prgSms;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(Logic.TAG, "On Create CountersActivity");
        super.onCreate(savedInstanceState);

        this.indivId = this.getIntent().getDoubleExtra("indiv_id", 0.0);

        log(String.format("Individual ID = %s", String.valueOf(this.indivId)));

        setContentView(R.layout.activity_counters);

        this.prgVoce = (ProgressBar) this.findViewById(R.id.prgVoce);
        this.prgDati = (ProgressBar) this.findViewById(R.id.prgDati);
        this.prgSms = (ProgressBar) this.findViewById(R.id.prgSms);

        this.settings = new Settings(this.getApplicationContext());

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        new CountersTask(this.settings.getInteractionPoint(1)).execute();
    }

    public void gotoOffers(View view)
    {
        try
        {
            callPostEvent();
        }
        catch (Exception exc)
        {

            Log.e(Logic.TAG, exc.getMessage());
        }

        // Start Offer activity
        Intent intent = new Intent(this, OfferActivity.class);
        startActivity(intent);
    }

    private void callPostEvent() throws Exception
    {

        String sessionId = session.getSessionId();
        String flowchart = this.settings.getIndivFlowchart();
        String eventName = this.settings.getPostEventName();

        log("Calling POST EVENT from Counters...");
        log("*** POST EVENT [BEGIN] ***");
        log("  > Session = " + sessionId);
        log("  > Counters = " + this.counters.toString());
        log("  > Flowchart Name = " + flowchart);
        log("  > Event Name = " + eventName);

        new PostEventTask(session, flowchart, eventName, this.counters).execute();

    }

    private void setProgressValue(ProgressBar progress, int value)
    {
        ProgressBarAnimation anim = new ProgressBarAnimation(progress, 0, value);
        anim.setDuration(500);
        progress.startAnimation(anim);
    }

    private void updateCounters(Counters counters)
    {

        final int MAX_SMS = 500;
        final int MAX_DATI = 500;
        final int MAX_VOCE = 400;

        this.prgSms.setMax(MAX_SMS);
        this.prgDati.setMax(MAX_DATI);
        this.prgVoce.setMax(MAX_VOCE);

        int sms = counters.getSMSValue();
        int voce = counters.getVoceValue();
        int dati = counters.getDatiValue();

        TextView lblSms = (TextView) this.findViewById(R.id.lblSms);
        TextView lblDati = (TextView) this.findViewById(R.id.lblDati);
        TextView lblVoce = (TextView) this.findViewById(R.id.lblVoce);

        String msgVoce = String.format("Traffico voce: (%s/%s min)", String.valueOf(voce), String.valueOf(MAX_VOCE));
        String msgDati = String.format("Traffico dati: (%s/%s MB)", String.valueOf(dati), String.valueOf(MAX_DATI));
        String msgSms = String.format("SMS Rimasti: %s di %s", String.valueOf(sms), String.valueOf(MAX_SMS));

        lblSms.setText(msgSms);
        lblDati.setText(msgDati);
        lblVoce.setText(msgVoce);

        this.setProgressValue(this.prgSms, sms);
        this.setProgressValue(this.prgDati, dati);
        this.setProgressValue(this.prgVoce, voce);

        // Set counters for Post Event
        this.counters = counters;
    }

    class CountersTask extends AsyncTask<Void, Void, Counters>
    {
        private InteractProxy proxy;
        private String interactionPoint;
        private ArrayList<AXSOfferList> offers;

        public CountersTask(String interactionPoint)
        {
            this.interactionPoint = interactionPoint;
            this.proxy = new InteractProxy();
            this.offers = null;
        }

        public Counters getCounters()
        {
            Counters counters = null;

            String vod_dati = InteractProxy.getAdditionalAttribute(this.offers, "VOD_DATI");
            String vod_sms = InteractProxy.getAdditionalAttribute(this.offers, "VOD_SMS");
            String vod_voce = InteractProxy.getAdditionalAttribute(this.offers, "VOD_VOCE");

            if (vod_dati != null && vod_sms != null && vod_voce != null)
            {
                counters = new Counters();
                counters.setSms(vod_sms);
                counters.setVoce(vod_voce);
                counters.setDati(vod_dati);
            }

            return counters;
        }

        public Counters perform()
        {
            AXSResponse response;
            Log.d(Logic.TAG, "Starting 2nd complete session...");

            int statusCode;

            try
            {

                // Get Offers for User
                log("Getting offer for IP CONTATORE...");
                log("*** GET OFFERS [BEGIN] ***");
                log("  > Session = " + session.getSessionId());
                log("  > Interaction Point = " + this.interactionPoint);
                response = this.proxy.getOffers(session.getSessionId(),
                        this.interactionPoint, 1);
                log("Request Status = " + response.statusCode);
                statusCode = response.statusCode;
                if (statusCode == 0)
                {
                    this.offers = response.allOfferLists;
                }
                log(InteractProxy.processResponse(response));
                if (this.offers != null)
                {
                    log(InteractProxy.processOffersList(this.offers));
                }
                else
                {
                    log("No offers found.");
                }
                log("*** GET OFFERS [END] ***");

            }
            catch (Exception exc)
            {
                Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
                log("** ERROR **");
                log(exc.getMessage());
            }

            log("Session ends.");

            return this.getCounters();

        }

        @Override
        protected Counters doInBackground(Void... params)
        {
            return this.perform();
        }

        @Override
        protected void onPostExecute(Counters counters)
        {
            if (counters != null)
            {
                log("Server responded with:");
                log(counters.toString());
                session.log().append("Counters Info = ").append(counters.toString());
                updateCounters(counters);
            }
            else
            {
                log("** ERROR ** Cannot find counters in this offer!");
            }
        }
    }

}
