/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Utils;

public class VersionTask extends AsyncTask<Void, Void, String>
{

    private Context context;
    private TextView statusbar;

    public VersionTask(Context ctx, TextView statusControl)
    {
        this.context = ctx;
        this.statusbar = statusControl;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        String version = null;

        Log.d(Logic.TAG, "Trying to connect to Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();
            version = proxy.getVersion();
            Log.d(Logic.TAG, "Version = " + version);
        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
        }

        return version;
    }

    @Override
    protected void onPostExecute(String result)
    {

        Log.d(Logic.TAG, "Server response...");

        if (Utils.isNotNullNotEmptyNotWhiteSpace(result))
        {
            statusbar.setText("Unica Interact Version > " + result);
            Log.i(Logic.TAG, "Connected");
        }
        else
        {
            statusbar.setText("Cannot connect!");
            CharSequence text;
            text = "Cannot connect to Interact (503)";
            Log.e(Logic.TAG, "Cannot connect to Interact (503)");
            Toast toast = Toast.makeText(this.context,
                    text, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}