/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSResponse;

import vodafone.ibm.com.it.vodafonepoc.Counters;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public class PostEventTask extends BasicTask
{

    private String flowchartName;
    private String eventName;
    private Counters counters;

    public PostEventTask(Session session, String flowchartName, String eventName, Counters counters)
    {
        super(session);
        this.flowchartName = flowchartName;
        this.eventName = eventName;
        this.counters = counters;
    }

    public String getInputDetails()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(" Flowchart Name = ").append(this.flowchartName);
        sb.append(System.lineSeparator());
        sb.append(" Event Name = ").append(String.valueOf(this.eventName));
        sb.append(System.lineSeparator());
        sb.append(" Counters => ").append(System.lineSeparator());
        sb.append(this.counters.toString()).append(System.lineSeparator());
        return sb.toString();
    }


    @Override
    public String perform()
    {
        AXSResponse response = null;
        Log.d(Logic.TAG, "Starting Post Event on Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();

            response = proxy.postEvent(this.session.getSessionId(),
                    this.eventName,
                    this.flowchartName,
                    this.counters.getAxsCounters());

            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            this.statusCode = response.statusCode;

        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
        }

        return InteractProxy.processResponse(response);
    }
}
