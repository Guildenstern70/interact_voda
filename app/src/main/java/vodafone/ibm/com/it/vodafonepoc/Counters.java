/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import com.unicacorp.interact.api.AXSNameValuePairImpl;

import java.util.ArrayList;

/**
 *
 */
public class Counters
{
    private String dati;
    private String voce;
    private String sms;

    public ArrayList<AXSNameValuePairImpl> getAxsCounters()
    {
        ArrayList<AXSNameValuePairImpl> counters = new ArrayList<>();

        AXSNameValuePairImpl dati = new AXSNameValuePairImpl();
        dati.name = "VOD_DATI";
        dati.valueAsString = this.dati;
        dati.valueDataType = "string";
        counters.add(dati);

        AXSNameValuePairImpl voce = new AXSNameValuePairImpl();
        voce.name = "VOD_VOCE";
        voce.valueAsString = this.voce;
        voce.valueDataType = "string";
        counters.add(voce);

        AXSNameValuePairImpl sms = new AXSNameValuePairImpl();
        sms.name = "VOD_SMS";
        sms.valueAsString = this.sms;
        sms.valueDataType = "string";
        counters.add(sms);

        return counters;
    }

    public String getDati()
    {
        return dati;
    }

    public int getDatiValue()
    {
        String datiValue = this.dati.substring(0, 3);
        Double datiDouble = (Double.parseDouble(datiValue) * 100.0);
        return datiDouble.intValue();
    }

    public void setDati(String dati)
    {
        this.dati = dati;
    }

    public String getVoce()
    {
        return voce;
    }

    public int getVoceValue()
    {
        String voceValue = this.voce.substring(0, 3);
        return Integer.parseInt(voceValue);
    }

    public void setVoce(String voce)
    {
        this.voce = voce;
    }

    public String getSms()
    {
        return sms;
    }

    public int getSMSValue()
    {
        String smsValue = this.sms.substring(0, 3);
        return Integer.parseInt(smsValue);
    }

    public void setSms(String sms)
    {
        this.sms = sms;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(" - Voce = " + this.getVoce()).append(System.lineSeparator());
        sb.append(" - Dati = " + this.getDati()).append(System.lineSeparator());
        sb.append(" - SMS  = " + this.getSms()).append(System.lineSeparator());
        return sb.toString();
    }
}
