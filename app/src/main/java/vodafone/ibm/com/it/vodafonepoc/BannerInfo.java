/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

/**
 *
 */
public class BannerInfo
{
    private String bannerId;
    private double individualId;

    public BannerInfo(String bannerId, double individualId)
    {
        this.bannerId = bannerId;
        this.individualId = individualId;
    }

    public String getBannerId()
    {
        return bannerId;
    }

    public void setBannerId(String bannerId)
    {
        this.bannerId = bannerId;
    }

    public double getIndividualId()
    {
        return individualId;
    }

    public void setIndividualId(double individualId)
    {
        this.individualId = individualId;
    }

    @Override
    public String toString()
    {
        return String.format("Banner Info: [%s, indiv=%s]", this.bannerId, String.valueOf(this.individualId));
    }
}
