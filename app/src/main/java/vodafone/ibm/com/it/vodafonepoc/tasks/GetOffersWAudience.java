/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSNameValuePairImpl;
import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;

import java.util.ArrayList;

import vodafone.ibm.com.it.vodafonepoc.Audience;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public class GetOffersWAudience extends BasicTask
{
    private InteractProxy proxy;
    private String interactionPoint;
    private StringBuilder log;
    private ArrayList<AXSOfferList> offers;
    private Audience audience;

    public GetOffersWAudience(Session session, Audience audience, String interactionPoint)
    {
        super(session);
        this.log = new StringBuilder();
        this.interactionPoint = interactionPoint;
        this.proxy = new InteractProxy();
        this.audience = audience;
        this.offers = null;
    }

    private double getProfile() throws Exception
    {
        this.log.append("*** GET PROFILE [BEGIN] ***").append(System.lineSeparator());
        String sessionId = this.session.getSessionId();
        Log.d(Logic.TAG, "Session ID = " + sessionId);
        AXSResponse response = this.proxy.getProfile(sessionId);
        Log.d(Logic.TAG, "Request Status = " + response.statusCode);
        AXSNameValuePairImpl individual = (AXSNameValuePairImpl) response.getProperty(5);
        double indiv_id = individual.valueAsNumeric;
        Log.d(Logic.TAG, " *Indiv_ID = " + String.valueOf(indiv_id));
        this.log.append(InteractProxy.processResponse(response));
        this.log.append("*** GET PROFILE [END] ***").append(System.lineSeparator());
        return indiv_id;
    }

    private void setAudience() throws Exception
    {
        this.log.append("*** SET AUDIENCE [BEGIN] ***").append(System.lineSeparator());
        AXSResponse response;
        Log.d(Logic.TAG, this.audience.toString());
        response = this.proxy.setAudience(this.session.getSessionId(), this.audience);
        Log.d(Logic.TAG, "Request Status = " + response.statusCode);
        Log.d(Logic.TAG, InteractProxy.processResponse(response));
        this.log.append(InteractProxy.processResponse(response));
        this.log.append("*** SET AUDIENCE [END] ***").append(System.lineSeparator());
    }

    @Override
    public String perform()
    {

        AXSResponse response;
        Log.d(Logic.TAG, "Starting complete session...");

        try
        {
            Log.d(Logic.TAG, "Getting profile...");
            double indiv_id = this.getProfile();
            Log.d(Logic.TAG, "Done: invid_id = " + String.valueOf(indiv_id));

            Log.d(Logic.TAG, "Setting audience to " + String.valueOf(indiv_id));
            this.audience.setAudienceIdValue(String.valueOf(indiv_id));
            this.setAudience();
            Log.d(Logic.TAG, "Done");

            // Get Offers for User
            Log.d(Logic.TAG, "Getting first offer...");
            this.log.append("*** GET OFFERS [BEGIN] ***").append(System.lineSeparator());
            response = this.proxy.getOffers(this.session.getSessionId(),
                    this.interactionPoint, 1);
            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            this.statusCode = response.statusCode;
            if (this.statusCode == 0)
            {
                this.offers = response.allOfferLists;
            }
            this.log.append(InteractProxy.processResponse(response));
            this.log.append(System.lineSeparator());
            if (this.offers != null)
            {
                this.log.append(InteractProxy.processOffersList(this.offers));
            }
            else
            {
                this.log.append("No offers found.");
            }
            this.log.append("*** GET OFFERS [END] ***").append(System.lineSeparator());

        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
            this.log.append("** ERROR **");
            this.log.append(exc.getMessage());
        }

        Log.d(Logic.TAG, "Session ends.");

        return this.log.toString();

    }
}
