/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

/**
 * Set Audience Param
 */
public class Audience
{
    private String flowchartName;
    private String audienceIdKey;
    private String audienceIdValue;
    private String audienceLevel;

    public String getFlowchartName()
    {
        return flowchartName;
    }

    public void setFlowchartName(String flowchartName)
    {
        this.flowchartName = flowchartName;
    }

    public String getAudienceIdKey()
    {
        return audienceIdKey;
    }

    public void setAudienceIdKey(String audienceIdKey)
    {
        this.audienceIdKey = audienceIdKey;
    }

    public String getAudienceIdValue()
    {
        return audienceIdValue;
    }

    public void setAudienceIdValue(String audienceIdValue)
    {
        this.audienceIdValue = audienceIdValue;
    }

    public String getAudienceLevel()
    {
        return audienceLevel;
    }

    public void setAudienceLevel(String audienceLevel)
    {
        this.audienceLevel = audienceLevel;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("Audience Params:").append(System.lineSeparator());
        sb.append(" - Flowchart Name    = ").append(this.flowchartName).append(System.lineSeparator());
        sb.append(" - Audience ID Key   = ").append(this.audienceIdKey).append(System.lineSeparator());
        sb.append(" - Audience ID Value = ").append(this.audienceIdValue).append(System.lineSeparator());
        sb.append(" - Audience Level    = ").append(this.audienceLevel).append(System.lineSeparator());
        return sb.toString();
    }
}
