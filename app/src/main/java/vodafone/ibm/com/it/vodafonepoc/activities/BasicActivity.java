/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.Session;
import vodafone.ibm.com.it.vodafonepoc.Settings;
import vodafone.ibm.com.it.vodafonepoc.tasks.BasicTask;

/**
 *
 */
public class BasicActivity extends Activity
{

    protected Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        this.session = Session.getInstance(this.getApplicationContext());
        super.onCreate(savedInstanceState);
    }

    protected void log(String message)
    {
        Log.d(Logic.TAG, message);
        session.log().append(message).append(System.lineSeparator());
    }


}
