/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */
package vodafone.ibm.com.it.vodafonepoc.activities.layout;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.activities.CustomListAdapter;


public class ClickItemLayout implements CustomListAdapter.CustomItemLayout
{

    @Override
    public View getView(Object item, Activity activity, CustomListAdapter adapter, int position)
    {
        LayoutInflater inflater = activity.getLayoutInflater();
        final View clickItemView = inflater.inflate(R.layout.click_item, null, true);
        TextView itemView = (TextView) clickItemView.findViewById(R.id.itemView);
        ClickItem clickItem = (ClickItem) item;
        itemView.setText(clickItem.getItemText());
        return clickItemView;
    }

    public static class ClickItem
    {
        private String itemText;

        public ClickItem(String itemText)
        {
            this.itemText = itemText;
        }

        public String getItemText()
        {
            return itemText;
        }
    }
}
