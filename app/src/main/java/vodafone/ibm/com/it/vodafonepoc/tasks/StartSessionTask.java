/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSResponse;

import vodafone.ibm.com.it.vodafonepoc.Audience;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 */
public class StartSessionTask extends BasicTask
{

    private Audience audience;
    private String interactiveChannel;

    public StartSessionTask(Session session, Audience audience, String interactiveChannel)
    {
        super(session);
        this.audience = audience;
        this.interactiveChannel = interactiveChannel;
    }

    public String getInputDetails()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(" Flowchart Name = " + audience.getFlowchartName());
        sb.append(System.lineSeparator());
        sb.append(" Interactive Channel = " + this.interactiveChannel);
        sb.append(System.lineSeparator());
        sb.append(" Audience ID Key = " + audience.getAudienceIdKey());
        sb.append(System.lineSeparator());
        sb.append(" Audience ID Value = " + audience.getAudienceIdValue());
        sb.append(System.lineSeparator());
        sb.append(" Audience Level = " + audience.getAudienceLevel());
        return sb.toString();
    }

    @Override
    public String perform()
    {
        StringBuilder sb = new StringBuilder("*** START SESSION [BEGIN] ***").append(System.lineSeparator());
        sb.append(this.getInputDetails());

        AXSResponse response = null;
        Log.d(Logic.TAG, "Starting Session on Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();
            response = proxy.startSession(this.session.getSessionId(),
                    audience.getFlowchartName(),
                    interactiveChannel,
                    audience.getAudienceIdKey(),
                    audience.getAudienceIdValue(),
                    audience.getAudienceLevel(),
                    false);
            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            this.statusCode = response.statusCode;
        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
            sb.append("** ERROR **");
            sb.append(exc.getMessage());
        }

        sb.append(InteractProxy.processResponse(response)).append(System.lineSeparator());
        sb.append("*** START SESSION [END] ***").append(System.lineSeparator());
        return sb.toString();
    }

}
