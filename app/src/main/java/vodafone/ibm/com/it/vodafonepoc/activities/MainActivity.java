/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unicacorp.interact.api.AXSNameValuePairImpl;
import com.unicacorp.interact.api.AXSResponse;

import java.util.concurrent.ExecutionException;

import vodafone.ibm.com.it.vodafonepoc.Audience;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.Settings;

/**
 *
 */
public class MainActivity extends BasicActivity
{
    private EditText txtUsername;
    private EditText txtPassword;
    private TextView lblUsername;
    private TextView lblPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(Logic.TAG, "On Create MainActivity");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        this.lblUsername = (TextView) findViewById(R.id.lblUsername);
        this.lblPassword = (TextView) findViewById(R.id.lblPassword);
        this.txtUsername = (EditText) findViewById(R.id.txtUsername);
        this.txtPassword = (EditText) findViewById(R.id.txtPassword);
        this.btnLogin = (Button) findViewById(R.id.btnLogin);
        this.btnLogin.setEnabled(true);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        Log.d(Logic.TAG, "OnPost Create MainActivity");
        super.onPostCreate(savedInstanceState);
        this.initializeUI();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.initializeUI();
        this.btnLogin.setEnabled(true);
        Log.d(Logic.TAG, "Resumed MainActivity");
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }

    private int startSession() throws InterruptedException, ExecutionException
    {
        this.session.newSession();
        Log.d(Logic.TAG, "Starting Interact New Session #" + this.session.getSessionId());

        // TODO User ID => Audience (Mobile User ID)
        String audienceValue = "x1LIrXQ9MRm7NSVj";

        Settings settings = new Settings(this.getApplicationContext());

        // Start Session Audience
        Audience startAudience = new Audience();
        startAudience.setFlowchartName(settings.getFlowchartName());
        startAudience.setAudienceIdKey(settings.getAudienceKey());
        startAudience.setAudienceIdValue(audienceValue);
        startAudience.setAudienceLevel(settings.getAudienceLevel());

        // Individual Audience (AudienceIdValue is taken from getProfile)
        Audience indivAudience = new Audience();
        indivAudience.setFlowchartName(settings.getIndivFlowchart());
        indivAudience.setAudienceIdKey(settings.getIndivAudienceKey());
        indivAudience.setAudienceLevel(settings.getIndivAudienceLevel());

        StartSessionInitTask task = new StartSessionInitTask(startAudience,
                indivAudience,
                settings.getInteractiveChannel());
        return task.execute().get();
    }

    public void gotoOffers()
    {
        boolean proceed = false;
        btnLogin.setEnabled(false);

        try
        {
            proceed = (this.startSession() == 0);
        }
        catch (InterruptedException | ExecutionException ex)
        {
        }

        if (proceed)
        {
            Intent intent = new Intent(this, OfferActivity.class);
            startActivity(intent);
        }
        else
        {
            CharSequence text = "Interact Server non disponibile";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getApplicationContext(), text, duration);
            toast.show();
        }

    }

    public void performLoginOrOffers(View view)
    {

        if (session.getUsername() != null)
        {
            Log.d(Logic.TAG, "Going to offers");
            session.newSession();
            this.gotoOffers();
            return;
        }

        Log.d(Logic.TAG, "Performing Login");

        String username = this.txtUsername.getText().toString();
        String password = this.txtPassword.getText().toString();

        CharSequence errorMessage = null;

        if (username.length() < 6)
        {
            errorMessage = "Il nome utente deve avere almeno 6 caratteri.";
        }
        else
        {
            if (Logic.checkLogin(username, password))
            {
                session.setUsername(username);
                Log.d(Logic.TAG, "Created session. ");
                Log.d(Logic.TAG, "Session ID = " + session.getSessionId());
                this.gotoOffers();
            }
            else
            {
                errorMessage = "Nome utente o password sbagliati.";
            }
        }

        if (errorMessage != null)
        {
            this.txtPassword.setText("");
            this.txtUsername.setText("");
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(getApplicationContext(), errorMessage, duration);
            toast.show();
            this.txtUsername.requestFocus();
        }
    }

    private void initializeUI()
    {

        String loggedUser = session.getUsername();
        Log.d(Logic.TAG, "Current user is = " + loggedUser);

        if (loggedUser != null)
        {
            LinearLayout layout = (LinearLayout) this.findViewById(R.id.fullscreen_content);
            layout.setGravity(Gravity.CENTER_HORIZONTAL);
            this.txtUsername.setVisibility(View.INVISIBLE);
            this.txtPassword.setVisibility(View.INVISIBLE);
            this.lblPassword.setText("Benvenuto, " + loggedUser + ".");
            this.btnLogin.setText("Offerte per te");
            this.lblUsername.setVisibility(View.INVISIBLE);
        }
    }

    class StartSessionInitTask extends AsyncTask<Void, Void, Integer>
    {

        private String interactiveChannel;
        private InteractProxy proxy;
        private Audience startAudience;
        private Audience indivAudience;

        public StartSessionInitTask(Audience startAudience,
                                    Audience indivAudience,
                                    String interactiveChannel)
        {
            this.startAudience = startAudience;
            this.indivAudience = indivAudience;
            this.interactiveChannel = interactiveChannel;
            this.proxy = new InteractProxy();
        }

        public String getInputDetails(Audience audience)
        {
            StringBuilder sb = new StringBuilder();
            sb.append("  Flowchart Name = ").append(audience.getFlowchartName());
            sb.append(System.lineSeparator());
            sb.append("  Interactive Channel = ").append(this.interactiveChannel);
            sb.append(System.lineSeparator());
            sb.append("  Audience ID Key = ").append(audience.getAudienceIdKey());
            sb.append(System.lineSeparator());
            sb.append("  Audience ID Value = ").append(audience.getAudienceIdValue());
            sb.append(System.lineSeparator());
            sb.append("  Audience Level = ").append(audience.getAudienceLevel());
            return sb.toString();
        }

        private double getProfile()
        {
            double indiv_id = -1.0;

            log("*** GET PROFILE [BEGIN] ***");
            String sessionId = session.getSessionId();
            Log.d(Logic.TAG, "Session ID = " + sessionId);

            try
            {
                AXSResponse response = this.proxy.getProfile(sessionId);
                Log.d(Logic.TAG, "Request Status = " + response.statusCode);
                AXSNameValuePairImpl individual = (AXSNameValuePairImpl) response.getProperty(5);
                indiv_id = individual.valueAsNumeric;
                Log.d(Logic.TAG, " *Indiv_ID = " + String.valueOf(indiv_id));
                log(InteractProxy.processResponse(response));
            }
            catch (Exception exc)
            {
                Log.e(Logic.TAG, exc.getMessage());
                log("** ERROR ** Error in getProfile");
            }
            finally
            {
                log("*** GET PROFILE [END] ***");
            }

            return indiv_id;
        }

        private void setAudience() throws Exception
        {
            log("*** SET AUDIENCE [BEGIN] ***");
            AXSResponse response;

            response = this.proxy.setAudience(session.getSessionId(), this.indivAudience);
            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            Log.d(Logic.TAG, InteractProxy.processResponse(response));
            log(InteractProxy.processResponse(response));
            log("*** SET AUDIENCE [END] ***");
        }

        public Integer perform()
        {
            int statusCode = -1;

            AXSResponse response;
            Log.d(Logic.TAG, "Starting Session on Interact server...");

            try
            {
                log("*** START SESSION [BEGIN] ***");
                log("Start Session Audience:");
                log(this.getInputDetails(startAudience));
                response = proxy.startSession(session.getSessionId(),
                        startAudience.getFlowchartName(),
                        interactiveChannel,
                        startAudience.getAudienceIdKey(),
                        startAudience.getAudienceIdValue(),
                        startAudience.getAudienceLevel(),
                        false);
                statusCode = response.statusCode;
                log(InteractProxy.processResponse(response));
                log("*** START SESSION [END] ***");

                if (statusCode == 0)
                {
                    log("Getting profile...");
                    double indiv_id = this.getProfile();
                    log("Done: invid_id = " + String.valueOf(indiv_id));

                    if (indiv_id > 0)
                    {
                        log("Setting audience to " + String.valueOf(indiv_id));
                        this.indivAudience.setAudienceIdValue(String.valueOf(indiv_id));
                        this.setAudience();
                        log("Setting audience done.");
                    }
                    else
                    {
                        log("** ERROR ** Unknown Indiv_id ");
                    }
                }

            }
            catch (Exception exc)
            {
                Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
                log("** ERROR **");
                log(exc.getMessage());
            }

            return statusCode;
        }

        @Override
        protected Integer doInBackground(Void... params)
        {
            return perform();
        }

        @Override
        protected void onPostExecute(Integer interactResponseCode)
        {
            log("Response code = " + interactResponseCode.toString());
        }

    }
}
