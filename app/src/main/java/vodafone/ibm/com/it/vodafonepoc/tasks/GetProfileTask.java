/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSNameValuePairImpl;
import com.unicacorp.interact.api.AXSResponse;

import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public class GetProfileTask extends BasicTask
{

    private double individual_id;

    public GetProfileTask(Session session)
    {
        super(session);
    }

    public double getIndividual_id()
    {
        return this.individual_id;
    }

    @Override
    public String perform()
    {
        AXSResponse response = null;
        Log.d(Logic.TAG, "Getting profile on Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();
            response = proxy.getProfile(this.session.getSessionId());
            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            AXSNameValuePairImpl individual = (AXSNameValuePairImpl) response.getProperty(5);
            this.individual_id = individual.valueAsNumeric;
            this.statusCode = response.statusCode;
        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
        }

        return InteractProxy.processResponse(response);
    }
}
