/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;

import java.util.ArrayList;

import vodafone.ibm.com.it.vodafonepoc.BannerInfo;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.Session;
import vodafone.ibm.com.it.vodafonepoc.Settings;


public class OfferActivity extends MenuActivity
{

    private TextView lblStatus;
    private ImageView imgBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(Logic.TAG, "On Create OfferActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        this.lblStatus = (TextView) findViewById(R.id.lblConnectionStatus);
        this.imgBanner = (ImageView) findViewById(R.id.imgOffer);

        this.lblStatus.setText("...");
    }

    @Override
    protected void onStart()
    {
        Log.d(Logic.TAG, "On Start OfferActivity");
        super.onStart();
        this.getOffers();
    }

    private void displayOffer()
    {
        BannerInfo bannerInfo = this.session.getBannerInfo();

        if (bannerInfo == null)
        {
            this.session.log().append("BannerID is null -> Offer#1");
            log("BannerID is null -> Offer#1");
            this.imgBanner.setImageResource(R.drawable.offerta0);
        }
        else if (bannerInfo.getBannerId().equals("12345"))
        {
            this.lblStatus.setText("Abbiamo un'offerta per te:");
            this.session.log().append("BannerID is 12345 -> Offer#2");
            log("BannerID is 12345 -> Offer#2");
            this.imgBanner.setImageResource(R.drawable.offerta2);
        }
        else
        {
            this.lblStatus.setText("C'è una nuova offerta per te:");
            this.session.log().append("BannerID is 54321 -> Offer#3");
            log("BannerID is 54321 -> Offer#3");
            this.imgBanner.setImageResource(R.drawable.offerta3);
        }

    }

    private void getOffers()
    {
        Settings settings = new Settings(this.getApplicationContext());
        this.lblStatus.setText("Getting Offers...");
        Log.d(Logic.TAG, "Get Offers Handler");
        new BannerTask(settings.getInteractionPoint(0)).execute();
    }

    public void callViewCounters(View view)
    {
        Intent intent = new Intent(this, CountersActivity.class);
        intent.putExtra("indiv_id", this.session.getBannerInfo().getIndividualId());
        startActivity(intent);
    }

    public void callViewLogs(View view)
    {
        Log.d(Logic.TAG, "callviewlogs - Session Logs:");
        Log.d(Logic.TAG, this.session.log().toString());

        Intent intent = new Intent(this, ViewLogActivity.class);
        startActivity(intent);
    }

    public Session getSession()
    {
        return session;
    }

    class BannerTask extends AsyncTask<Void, Void, BannerInfo>
    {
        private InteractProxy proxy;
        private String interactionPoint;
        private ArrayList<AXSOfferList> offers;

        public BannerTask(String interactionPoint)
        {
            this.interactionPoint = interactionPoint;
            this.proxy = new InteractProxy();
            this.offers = null;
        }

        public String getBannerID()
        {
            String bannerId = InteractProxy.getAdditionalAttribute(this.offers, "BANNERID");
            log("Banner ID = " + bannerId);
            return bannerId;
        }

        public BannerInfo perform()
        {
            AXSResponse response;
            log("Starting GetOffer session...");

            int statusCode;
            double indivId = -1;
            String bannerId = null;

            try
            {
                // Get Offers for User
                log("Getting first offer...");
                log("*** GET OFFERS [BEGIN] ***");
                response = this.proxy.getOffers(session.getSessionId(),
                        this.interactionPoint, 1);
                log("Request Status = " + response.statusCode);
                statusCode = response.statusCode;
                if (statusCode == 0)
                {
                    this.offers = response.allOfferLists;
                }
                log(InteractProxy.processResponse(response));
                if (this.offers != null)
                {
                    log(InteractProxy.processOffersList(this.offers));
                }
                else
                {
                    log("No offers found.");
                }
                log("*** GET OFFERS [END] ***");
                bannerId = this.getBannerID();
            }
            catch (Exception exc)
            {
                Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
                log("** ERROR **");
                log(exc.getMessage());
            }

            log("Session ends.");
            return new BannerInfo(bannerId, indivId);

        }

        @Override
        protected BannerInfo doInBackground(Void... params)
        {
            return this.perform();
        }

        @Override
        protected void onPostExecute(BannerInfo bannerInfo)
        {
            if (bannerInfo != null)
            {
                log("Banner Info = ");
                log(bannerInfo.toString());
                session.setBannerInfo(bannerInfo);
                displayOffer();
            }
            else
            {
                log("** ERROR ** This offer contained no banner.");
            }
        }

    }

}
