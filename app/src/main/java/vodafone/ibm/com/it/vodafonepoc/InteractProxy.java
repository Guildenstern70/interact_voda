/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import com.unicacorp.interact.api.AXSAdvisoryMessage;
import com.unicacorp.interact.api.AXSInteractSoapServiceSoap11Binding;
import com.unicacorp.interact.api.AXSNameValuePairImpl;
import com.unicacorp.interact.api.AXSOffer;
import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;
import com.unicacorp.interact.api.AXSpostEvent;
import com.unicacorp.interact.api.AXSsetAudience;
import com.unicacorp.interact.api.AXSstartSession;
import com.unicacorp.interact.api.InteractSoapWrapper;

import java.util.ArrayList;

/**
 *
 */
public class InteractProxy
{

    private static final String INTERACT_SERVER_IP = "185.64.246.229";
    private static final String INTERACT_SERVER_PORT = "7001";

    private static final String NEWLINE = System.getProperty("line.separator");

    private InteractSoapWrapper service;

    /**
     * Constructor
     */
    public InteractProxy()
    {
        this.service = new InteractSoapWrapper(INTERACT_SERVER_IP, INTERACT_SERVER_PORT);
        this.service.enableLogging = true;
    }

    /**
     * Constructor
     */
    public InteractProxy(String ip, String port)
    {
        this.service = new InteractSoapWrapper(ip, port);
        this.service.enableLogging = true;
    }

    /**
     * Get version
     *
     * @return Interact API version
     * @throws Exception
     */
    public String getVersion() throws Exception
    {
        AXSResponse response = this.service.getVersion();
        return response.apiVersion;
    }

    public static String getAdditionalAttribute(ArrayList<AXSOfferList> offers, String key)
    {
        String foundValue = null;

        if (offers != null && offers.size() > 0)
        {
            AXSOfferList of = offers.get(0);

            if (of.recommendedOffers.size() > 0)
            {
                AXSOffer offer = of.recommendedOffers.get(0);

                ArrayList<AXSNameValuePairImpl> offerValues = offer.additionalAttributes;
                for (AXSNameValuePairImpl nvp : offerValues)
                {
                    String nvpKey = nvp.name;
                    String nvpValue = nvp.valueAsString;

                    if (nvpKey.equals(key))
                    {
                        foundValue = nvpValue;
                        break;
                    }
                }

            }

        }

        return foundValue;

    }

    public static String processOffersList(ArrayList<AXSOfferList> offers)
    {

        StringBuilder sb = new StringBuilder();

        sb.append("  Offers     #> ").append(offers.size());
        sb.append(NEWLINE);
        for (AXSOfferList of : offers)
        {
            sb.append("  > Offer = " + of.defaultString).append(NEWLINE);
            sb.append("  > Offer Interaction Point = " + of.interactionPointName).append(NEWLINE);
            sb.append("  > Recommended Offers # " + of.recommendedOffers.size()).append(NEWLINE);
            for (AXSOffer offer : of.recommendedOffers)
            {
                sb.append("  > OFFER " + offer.offerName).append(NEWLINE);
                String treatmentCode = offer.treatmentCode;
                sb.append("  > TREATMENT CODE " + treatmentCode).append(NEWLINE);
                ArrayList<String> offerCodes = offer.offerCode;
                for (String offerCode : offerCodes)
                {
                    sb.append("  > => OFFER CODE # " + offerCode).append(NEWLINE);
                }
                ArrayList<AXSNameValuePairImpl> offerValues = offer.additionalAttributes;
                if (treatmentCode != null)
                {
                    if (offerValues != null)
                    {
                        sb.append("  > Additional Attributes:").append(NEWLINE);
                        for (AXSNameValuePairImpl nvp : offerValues)
                        {
                            String nvpKey = nvp.name;
                            String nvpValue = nvp.valueAsString;

                            sb.append("  *> " + nvpKey + " = " + nvpValue).append(NEWLINE);
                        }
                    }
                    else
                    {
                        sb.append("  > No additional attributes found.").append(NEWLINE);
                    }
                }

            }
        }

        return sb.toString();
    }

    /**
     * Build a human readable response from Interact response
     *
     * @param response
     * @return a human readable response from Interact response
     */
    public static String processResponse(AXSResponse response)
    {
        StringBuilder sb = new StringBuilder();

        sb.append("  Session ID > ").append(response.sessionID);
        sb.append(NEWLINE);

        switch (response.statusCode)
        {
            case 0: // Success
                sb.append("  Status > SUCCESS");
                break;

            case 1: // Warning
                sb.append("  Status > WARNING");
                break;

            case 2: // Error
                sb.append("  Status > ERROR");
                break;
        }

        sb.append(NEWLINE);

        sb.append("  Properties  > ").append(String.valueOf(response.getPropertyCount()));
        sb.append(NEWLINE);

        for (int k = 0; k < response.getPropertyCount(); k++)
        {
            Object property = response.getProperty(k);
            if (property instanceof AXSNameValuePairImpl)
            {
                AXSNameValuePairImpl kvProp = (AXSNameValuePairImpl) property;
                if (kvProp.valueDataType.equals("numeric"))
                {
                    sb.append(">> [" + kvProp.name + " = " + kvProp.valueAsNumeric + "]");
                }
                else
                {
                    sb.append(">> [" + kvProp.name + " = " + kvProp.valueAsString + "]");
                }
            }
            else
            {
                sb.append("        >>> " + response.getProperty(k).toString());
            }
            sb.append(NEWLINE);
        }

        if (response.statusCode != 0)
        {
            sb.append("* Errors found*");
            sb.append(NEWLINE);
            for (AXSAdvisoryMessage am : response.advisoryMessages)
            {
                sb.append(NEWLINE);
                sb.append("   Error #");
                sb.append(am.messageCode);
                sb.append(NEWLINE);
                sb.append("   > ").append(am.message);
                sb.append(NEWLINE);
                sb.append("   > ").append(am.detailMessage);
                sb.append(NEWLINE);
            }
        }

        sb.append(NEWLINE);

        return sb.toString();
    }

    /**
     * Start Session
     *
     * @param sessionId
     * @param flowchartName
     * @param interactiveChannel
     * @param audienceLevel
     * @param audienceIdKey
     * @param audienceIdValue
     * @param relyExistingSession
     * @return
     * @throws Exception
     */
    public AXSResponse startSession(String sessionId, String flowchartName, String interactiveChannel,
                                    String audienceIdKey, String audienceIdValue, String audienceLevel,
                                    boolean relyExistingSession) throws Exception
    {
        AXSstartSession startSession = new AXSstartSession();
        startSession.sessionID = sessionId;
        startSession.relyOnExistingSession = relyExistingSession;
        startSession.interactiveChannel = interactiveChannel;
        startSession.audienceLevel = audienceLevel;

        // Audience ID
        startSession.audienceID.add(this.getParamAudienceId(audienceIdKey, audienceIdValue));

        // Debug Mode
        startSession.debug = true;

        // Flowchart
        startSession.parameters.add(this.getParamFlowchart(flowchartName));

        return this.service.startSession(startSession);

    }

    /**
     * Get Profile
     *
     * @param sessionId
     * @return
     * @throws Exception
     */
    public AXSResponse getProfile(String sessionId) throws Exception
    {
        return this.service.getProfile(sessionId);
    }


    /**
     * Set Audience
     *
     * @param sessionId
     * @param audience
     * @return
     * @throws Exception
     * @see Audience
     */
    public AXSResponse setAudience(String sessionId, Audience audience) throws Exception
    {
        AXSsetAudience setAudienceRequest = new AXSsetAudience();
        setAudienceRequest.sessionID = sessionId;
        setAudienceRequest.audienceLevel = audience.getAudienceLevel();

        // Audience ID
        double audienceIdValueDbl = -1;
        try
        {
            audienceIdValueDbl = Double.parseDouble(audience.getAudienceIdValue());
            setAudienceRequest.audienceID.add(this.getParamAudienceId(
                    audience.getAudienceIdKey(), audienceIdValueDbl));
        }
        catch (NumberFormatException nex)
        {
            setAudienceRequest.audienceID.add(this.getParamAudienceId(
                    audience.getAudienceIdKey(), audience.getAudienceIdValue()));
        }

        // Flowchart
        setAudienceRequest.parameters.add(this.getParamFlowchart(audience.getFlowchartName()));

        return this.service.setAudience(setAudienceRequest);

    }

    /**
     * Get Offers
     *
     * @param sessionId
     * @param interactionPoint
     * @param numberRequested
     * @return
     * @throws Exception
     */
    public AXSResponse getOffers(String sessionId, String interactionPoint, int numberRequested) throws Exception
    {
        return this.service.getOffers(sessionId, interactionPoint, numberRequested);
    }


    public AXSResponse postEvent(String sessionId, String eventName, String flowchartName,
                                 ArrayList<AXSNameValuePairImpl> counters) throws Exception
    {
        AXSpostEvent postEvent = new AXSpostEvent();
        postEvent.sessionID = sessionId;
        postEvent.eventName = eventName;

        // Flowchart
        postEvent.eventParameters.add(this.getParamFlowchart(flowchartName));

        // Counters
        for (AXSNameValuePairImpl counter : counters)
        {
            postEvent.eventParameters.add(counter);
        }

        return this.service.postEvent(postEvent);
    }


    private AXSNameValuePairImpl getParamFlowchart(String flowchartName)
    {
        AXSNameValuePairImpl flowchart = new AXSNameValuePairImpl();
        flowchart.name = "UACIExecuteFlowchartByName";
        flowchart.valueDataType = "string";
        flowchart.valueAsString = flowchartName;
        return flowchart;
    }

    private AXSNameValuePairImpl getParamAudienceId(String idKey, double idValue)
    {
        AXSNameValuePairImpl audienceId0 = new AXSNameValuePairImpl();
        audienceId0.name = idKey;
        audienceId0.valueDataType = "numeric";
        audienceId0.valueAsNumeric = idValue;
        return audienceId0;
    }

    private AXSNameValuePairImpl getParamAudienceId(String idKey, String idValue)
    {
        AXSNameValuePairImpl audienceId0 = new AXSNameValuePairImpl();
        audienceId0.name = idKey;
        audienceId0.valueDataType = "string";
        audienceId0.valueAsString = idValue;
        return audienceId0;
    }


}