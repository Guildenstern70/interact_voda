/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Settings is a proxy for Preferences
 */
public class Settings
{

    private Context applicationContext;
    private SharedPreferences preferences;

    public Settings(Context appContext)
    {
        this.applicationContext = appContext;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(this.applicationContext);
    }

    public String getUsername()
    {
        return this.preferences.getString("username", "?");
    }

    public void setUsername(String username)
    {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString("username", username);
        Log.d(Logic.TAG, "Saving username = " + username);
        editor.apply();
    }

    public String getMceUserId()
    {
        return this.preferences.getString("mceuserid", "?");
    }

    public void setMceUserId(String mceUserId)
    {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString("mceuserid", mceUserId);
        Log.d(Logic.TAG, "Saving MCE User Id = " + mceUserId);
        editor.apply();
    }

    public void removeUserPreferences()
    {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putString("mceuserid", "?");
        editor.putString("username", "?");
        editor.apply();
    }

    public String getServerURL()
    {
        return this.preferences.getString("serverUrl", "185.64.246.229");
    }

    public String getInteractiveChannel()
    {
        return this.preferences.getString("interactiveChannel", "VODAFONE");
    }

    public String getFlowchartName()
    {
        return this.preferences.getString("flowchartName", "SEG_MUID");
    }

    public String getAudienceLevel()
    {
        return this.preferences.getString("audienceLevel", "MUID");
    }

    public String getAudienceKey()
    {
        return this.preferences.getString("audienceKey", "MUID");
    }

    public String getIndivFlowchart()
    {
        return this.preferences.getString("individualFlowChart", "SEG_INDIVIDUAL");
    }

    public String getIndivAudienceLevel()
    {
        return this.preferences.getString("individualAudienceLevel", "Individual");
    }

    public String getIndivAudienceKey()
    {
        return this.preferences.getString("individualAudienceKey", "indiv_id");
    }

    public String getPostEventName()
    {
        return this.preferences.getString("postEventName", "check_contatori");
    }

    public String getInteractionPoint(int index)
    {
        String interactionPoint;

        switch (index)
        {
            case 0:
            default:
                interactionPoint = this.preferences.getString("interactionPoint1", "HOME");
                break;

            case 1:
                interactionPoint = this.preferences.getString("interactionPoint2", "CONTATORE");
                break;
        }

        return interactionPoint;
    }

    public int getNrOfOffers()
    {
        return this.preferences.getInt("nrOfOffers", 1);
    }

}
