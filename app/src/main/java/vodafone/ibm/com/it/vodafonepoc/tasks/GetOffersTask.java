/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.util.Log;

import com.unicacorp.interact.api.AXSOffer;
import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;

import java.util.ArrayList;

import vodafone.ibm.com.it.vodafonepoc.Counters;
import vodafone.ibm.com.it.vodafonepoc.InteractProxy;
import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public class GetOffersTask extends BasicTask
{

    private String interactionPoint;
    private int offersRequested;
    ArrayList<AXSOfferList> offers;

    public GetOffersTask(Session session, String interactionPoint, int numberRequested)
    {
        super(session);
        this.offers = null;
        this.interactionPoint = interactionPoint;
        this.offersRequested = numberRequested;
    }

    public String getInputDetails()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(" Interaction Point = ").append(this.interactionPoint);
        sb.append(System.lineSeparator());
        sb.append(" Offers Requested = ").append(String.valueOf(this.offersRequested));
        return sb.toString();
    }

    public ArrayList<AXSOfferList> getOffersList()
    {
        return this.offers;
    }

    public String getBannerID()
    {
        String bannerId = InteractProxy.getAdditionalAttribute(this.offers, "BANNERID");
        Log.d(Logic.TAG, "Banner ID = " + bannerId);
        return bannerId;
    }

    public Counters getCounters()
    {
        Counters counters = null;

        String vod_dati = InteractProxy.getAdditionalAttribute(this.offers, "VOD_DATI");
        String vod_sms = InteractProxy.getAdditionalAttribute(this.offers, "VOD_SMS");
        String vod_voce = InteractProxy.getAdditionalAttribute(this.offers, "VOD_VOCE");

        if (vod_dati != null && vod_sms != null && vod_voce != null)
        {
            counters = new Counters();
            counters.setSms(vod_sms);
            counters.setVoce(vod_voce);
            counters.setDati(vod_dati);
        }

        return counters;
    }

    @Override
    public String perform()
    {
        AXSResponse response = null;
        Log.d(Logic.TAG, "Starting Session on Interact server...");

        try
        {
            InteractProxy proxy = new InteractProxy();

            response = proxy.getOffers(this.session.getSessionId(),
                    this.interactionPoint, this.offersRequested);

            Log.d(Logic.TAG, "Request Status = " + response.statusCode);
            this.statusCode = response.statusCode;
            this.offers = response.allOfferLists;

        }
        catch (Exception exc)
        {
            Log.e(Logic.TAG, "Cannot connect to Interact server", exc);
        }

        return InteractProxy.processResponse(response);
    }
}
