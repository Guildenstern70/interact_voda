/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import java.util.Random;

/**
 * Business Logic
 */
public class Logic
{
    public static String TAG = "**VFPOC**";

    public static String INTERACT_URL = "http://185.64.246.229:7001";

    public static boolean checkLogin(String username, String password)
    {

        if (username.length() > 0 && password.length() > 0)
        {
            if (password.toLowerCase().equals("vfibm"))
            {
                return true;
            }
        }

        return false;

    }

    public static String generateCode(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }
}
