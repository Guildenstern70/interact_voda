/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import android.util.Log;

import com.ibm.mce.sdk.api.MceApplication;
import com.ibm.mce.sdk.api.MceSdk;
import com.ibm.mce.sdk.api.notification.MceNotificationActionRegistry;
import com.ibm.mce.sdk.plugin.displayweb.DisplayWebViewAction;
import com.ibm.mce.sdk.plugin.inbox.HtmlRichContent;
import com.ibm.mce.sdk.plugin.inbox.RichContentTemplateRegistry;

/**
 *
 */
public class VodafonePoc extends MceApplication
{

    @Override
    public void onCreate()
    {
        super.onCreate();

        Log.d(Logic.TAG, "Welcome to VodafonePOC - An MCE Application");

        RichContentTemplateRegistry.registerTemplate("default", new HtmlRichContent());

        MceNotificationActionRegistry.registerNotificationAction("displayWebView", new DisplayWebViewAction());

        /**
         * Custom layout
         */

        // MceSdk.getNotificationsClient().setCustomNotificationLayout(this, getResources().getString(
        //     R.string.expandable_layout_type), R.layout.custom_notification,
        //     R.id.bigText, R.id.bigImage, R.id.action1, R.id.action2, R.id.action3);

        MceSdk.getNotificationsClient().getNotificationsPreference().setSoundEnabled(getApplicationContext(), true);
        MceSdk.getNotificationsClient().getNotificationsPreference().setSound(getApplicationContext(), R.raw.notification_sound);
        MceSdk.getNotificationsClient().getNotificationsPreference().setVibrateEnabled(getApplicationContext(), true);
        long[] vibrate = {0, 100, 200, 300};
        MceSdk.getNotificationsClient().getNotificationsPreference().setVibrationPattern(getApplicationContext(), vibrate);
        MceSdk.getNotificationsClient().getNotificationsPreference().setIcon(getApplicationContext(), R.drawable.icon);
        MceSdk.getNotificationsClient().getNotificationsPreference().setLightsEnabled(getApplicationContext(), true);
        int ledARGB = 0x00a2ff;
        int ledOnMS = 300;
        int ledOffMS = 1000;
        MceSdk.getNotificationsClient().getNotificationsPreference().setLights(getApplicationContext(), new int[]{ledARGB, ledOnMS, ledOffMS});

    }


}
