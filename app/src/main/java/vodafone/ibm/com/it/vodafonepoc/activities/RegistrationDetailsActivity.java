/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.ibm.mce.sdk.api.MceSdk;
import com.ibm.mce.sdk.api.registration.RegistrationDetails;

import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.R;
import vodafone.ibm.com.it.vodafonepoc.Utils;
import vodafone.ibm.com.it.vodafonepoc.activities.layout.KeyValueLayout;

/**
 *
 */
public class RegistrationDetailsActivity extends MenuActivity
{
    private static final int[] REGISTRATION_DETAILS_RESOURCES_IDS =
            {R.string.user_id_title, R.string.channel_id_title, R.string.appkey_title};

    private static final int USER_ID_INDEX = 0;
    private static final int CHANNEL_ID_INDEX = 1;
    private static final int APPKEY_INDEX = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(Logic.TAG, "On Create OfferActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_details);
        this.showRegistrationDetails();
    }

    private void showRegistrationDetails()
    {

        ListView listView = (ListView) findViewById(R.id.listView2);
        String notRegistered = getResources().getString(R.string.not_registered);
        String[] values = Utils.resourcesToStringArray(getResources(), REGISTRATION_DETAILS_RESOURCES_IDS);
        KeyValueLayout.KeyValue<String>[] uiValues = new KeyValueLayout.KeyValue[values.length];
        RegistrationDetails registrationDetails =
                MceSdk.getRegistrationClient().getRegistrationDetails(getApplicationContext());

        String userId = registrationDetails.getUserId() !=
                null ? registrationDetails.getUserId() : notRegistered;

        Log.d(Logic.TAG, "MCE User ID = " + userId);

        String channelId = registrationDetails.getChannelId() !=
                null ? registrationDetails.getChannelId() : notRegistered;

        Log.d(Logic.TAG, "MCE Channel ID = " + channelId);

        uiValues[USER_ID_INDEX] = new KeyValueLayout.KeyValueString(values[USER_ID_INDEX], userId);
        uiValues[CHANNEL_ID_INDEX] = new KeyValueLayout.KeyValueString(values[CHANNEL_ID_INDEX], channelId);
        uiValues[APPKEY_INDEX] = new KeyValueLayout.KeyValueString(values[APPKEY_INDEX], MceSdk.getRegistrationClient().getAppKey(getApplicationContext()));

        CustomListAdapter adapter = new CustomListAdapter(this, null, uiValues);

        listView.setAdapter(adapter);
    }
}
