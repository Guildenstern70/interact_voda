/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.activities;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import vodafone.ibm.com.it.vodafonepoc.R;

/**
 *
 */
public class MenuActivity extends BasicActivity
{
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Intent intent;
        int id = item.getItemId();
        boolean result;

        switch (id)
        {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                result = true;
                break;

            case R.id.action_viewlog:
                intent = new Intent(this, ViewLogActivity.class);
                startActivity(intent);
                result = true;
                break;

            case R.id.action_reg_details:
                intent = new Intent(this, RegistrationDetailsActivity.class);
                startActivity(intent);
                result = true;
                break;

            case R.id.action_logout:
                this.session.logOut();
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                result = true;
                break;


            default:
                result = super.onOptionsItemSelected(item);
        }

        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
