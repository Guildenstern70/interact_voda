/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import android.content.res.Resources;

import java.text.DecimalFormat;

public class Utils
{

    public static String[] resourcesToStringArray(Resources res, int[] resourcesIds)
    {
        String[] stringArray = new String[resourcesIds.length];
        for (int i = 0; i < stringArray.length; ++i)
        {
            stringArray[i] = res.getString(resourcesIds[i]);
        }
        return stringArray;
    }


    /**
     * If the string is null or empty (or whitespace)
     *
     * @param string
     * @return
     */
    public static boolean isNotNullNotEmptyNotWhiteSpace(final String string)
    {
        return string != null && !string.isEmpty() && !string.trim().isEmpty();
    }

    public static String formatFromDouble(Double arg)
    {
        String retStr = null;

        if (arg != null)
        {
            try
            {
                retStr = new DecimalFormat("#").format(arg);
            }
            catch (ClassCastException exc)
            {
                retStr = arg.toString();
            }
        }

        return retStr;
    }

}
