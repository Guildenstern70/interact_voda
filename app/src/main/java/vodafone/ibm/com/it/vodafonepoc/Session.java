/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import android.content.Context;
import android.preference.PreferenceManager;

import java.util.Random;
import java.util.Set;

/**
 *
 */
public class Session
{
    private static Session instance = null;

    private String username;
    private String sessionId;
    private StringBuilder sessionLog;
    private BannerInfo banner;
    private Settings settings;

    private Session(Context ctx)
    {
        this.username = null;
        this.sessionLog = new StringBuilder("Vodafone POC").append(System.lineSeparator());
        this.sessionLog.append("============").append(System.lineSeparator());
        this.generateNewSessionId();
        if (ctx != null)
        {
            this.settings = new Settings(ctx);
        }
    }

    public void logOut()
    {
        this.username = null;
        this.settings.removeUserPreferences();
    }

    public static Session getInstance(Context ctx)
    {
        if (instance == null)
        {
            instance = new Session(ctx);
        }
        return instance;
    }

    public StringBuilder log()
    {
        return this.sessionLog;
    }

    public void newSession()
    {
        this.generateNewSessionId();
    }

    public String getSessionId()
    {
        return this.sessionId;
    }

    public String getUsername()
    {
        if (this.username == null)
        {
            String savedUsername = this.settings.getUsername();
            if (!savedUsername.equals("?"))
            {
                this.username = savedUsername;
            }
        }
        return this.username;
    }

    public void setUsername(String name)
    {
        this.username = name;
        this.settings.setUsername(name);
    }

    private void generateNewSessionId()
    {
        this.sessionLog = new StringBuilder();
        this.sessionId = Logic.generateCode(new Random(), "ABDEFG01234567", 10);
    }

    public BannerInfo getBannerInfo()
    {
        return banner;
    }

    public void setBannerInfo(BannerInfo bannerId)
    {
        this.banner = bannerId;
    }

}
