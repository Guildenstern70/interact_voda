/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.unicacorp.interact.api.AXSResponse;

import vodafone.ibm.com.it.vodafonepoc.Logic;
import vodafone.ibm.com.it.vodafonepoc.Session;

/**
 *
 */
public abstract class BasicTask extends AsyncTask<Void, Void, String>
{

    protected int statusCode;

    protected TextView statusBar;
    protected EditText statusDetails;
    protected Session session;

    public BasicTask(Session session)
    {
        this.statusCode = -1;
        this.session = session;
    }

    public String perform()
    {
        return null;
    }

    public void setUIControls(TextView statusBar, EditText statusDetails)
    {
        this.statusDetails = statusDetails;
        this.statusBar = statusBar;
    }

    public int getStatusCode()
    {
        return this.statusCode;
    }

    @Override
    protected String doInBackground(Void... params)
    {
        return perform();
    }

    @Override
    protected void onPostExecute(String detailedResult)
    {
        Log.d(Logic.TAG, "Server responded with:");

        if (this.statusBar != null)
        {
            statusBar.setText("Interact Response Received");
        }

        Log.d(Logic.TAG, detailedResult);
        if (this.statusDetails != null)
        {
            this.statusDetails.setText(detailedResult);
        }
        this.session.log().append(detailedResult);
    }
}
