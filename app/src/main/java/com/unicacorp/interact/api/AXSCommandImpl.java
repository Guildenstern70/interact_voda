/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;

import org.ksoap2.serialization.*;

import java.util.ArrayList;

import org.ksoap2.serialization.PropertyInfo;

public class AXSCommandImpl extends AttributeContainer implements KvmSerializable
{


    public ArrayList<AXSNameValuePairImpl> audienceID = new ArrayList<AXSNameValuePairImpl>();

    public String audienceLevel;

    public Boolean debug;

    public String _event;

    public ArrayList<AXSNameValuePairImpl> eventParameters = new ArrayList<AXSNameValuePairImpl>();

    public ArrayList<AXSGetOfferRequest> getOfferRequests = new ArrayList<AXSGetOfferRequest>();

    public String interactionPoint;

    public String interactiveChannel;

    public String methodIdentifier;

    public Integer numberRequested;

    public Boolean relyOnExistingSession;

    public AXSCommandImpl()
    {
    }

    public AXSCommandImpl(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue();
                if (info.name.equals("audienceID"))
                {

                    if (this.audienceID == null)
                    {
                        this.audienceID = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.audienceID.add(j1);

                    continue;
                }
                if (info.name.equals("audienceLevel"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.audienceLevel = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.audienceLevel = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("debug"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.debug = new Boolean(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Boolean)
                    {
                        this.debug = (Boolean) obj;
                    }
                    continue;
                }
                if (info.name.equals("event"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this._event = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this._event = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("eventParameters"))
                {

                    if (this.eventParameters == null)
                    {
                        this.eventParameters = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.eventParameters.add(j1);

                    continue;
                }
                if (info.name.equals("getOfferRequests"))
                {

                    if (this.getOfferRequests == null)
                    {
                        this.getOfferRequests = new ArrayList<AXSGetOfferRequest>();
                    }
                    java.lang.Object j = obj;
                    AXSGetOfferRequest j1 = (AXSGetOfferRequest) __envelope.get(j, AXSGetOfferRequest.class);
                    this.getOfferRequests.add(j1);

                    continue;
                }
                if (info.name.equals("interactionPoint"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.interactionPoint = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.interactionPoint = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("interactiveChannel"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.interactiveChannel = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.interactiveChannel = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("methodIdentifier"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.methodIdentifier = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.methodIdentifier = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("numberRequested"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.numberRequested = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.numberRequested = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("relyOnExistingSession"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.relyOnExistingSession = new Boolean(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Boolean)
                    {
                        this.relyOnExistingSession = (Boolean) obj;
                    }
                    continue;
                }

            }

        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.audienceLevel != null ? this.audienceLevel : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.debug != null ? this.debug : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this._event != null ? this._event : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.interactionPoint != null ? this.interactionPoint : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 4)
        {
            return this.interactiveChannel != null ? this.interactiveChannel : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 5)
        {
            return this.methodIdentifier != null ? this.methodIdentifier : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 6)
        {
            return this.numberRequested != null ? this.numberRequested : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 7)
        {
            return this.relyOnExistingSession != null ? this.relyOnExistingSession : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +8 && propertyIndex < +8 + this.audienceID.size())
        {
            return this.audienceID.get(propertyIndex - (+8));
        }
        if (propertyIndex >= +8 + this.audienceID.size() && propertyIndex < +8 + this.audienceID.size() + this.eventParameters.size())
        {
            return this.eventParameters.get(propertyIndex - (+8 + this.audienceID.size()));
        }
        if (propertyIndex >= +8 + this.audienceID.size() + this.eventParameters.size() && propertyIndex < +8 + this.audienceID.size() + this.eventParameters.size() + this.getOfferRequests.size())
        {
            return this.getOfferRequests.get(propertyIndex - (+8 + this.audienceID.size() + this.eventParameters.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 8 + audienceID.size() + eventParameters.size() + getOfferRequests.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "audienceLevel";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "debug";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "event";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "interactionPoint";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "interactiveChannel";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 5)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "methodIdentifier";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 6)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "numberRequested";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 7)
        {
            info.type = PropertyInfo.BOOLEAN_CLASS;
            info.name = "relyOnExistingSession";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +8 && propertyIndex <= +8 + this.audienceID.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "audienceID";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +8 + this.audienceID.size() && propertyIndex <= +8 + this.audienceID.size() + this.eventParameters.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "eventParameters";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +8 + this.audienceID.size() + this.eventParameters.size() && propertyIndex <= +8 + this.audienceID.size() + this.eventParameters.size() + this.getOfferRequests.size())
        {
            info.type = AXSGetOfferRequest.class;
            info.name = "getOfferRequests";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
