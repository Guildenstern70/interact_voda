/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class AXSBatchResponse extends AttributeContainer implements KvmSerializable
{


    public Integer batchStatusCode;

    public ArrayList<AXSResponse> responses = new ArrayList<AXSResponse>();

    public AXSBatchResponse()
    {
    }

    public AXSBatchResponse(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("batchStatusCode"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.batchStatusCode = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.batchStatusCode = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("responses"))
                {

                    if (this.responses == null)
                    {
                        this.responses = new ArrayList<AXSResponse>();
                    }
                    java.lang.Object j = obj;
                    AXSResponse j1 = (AXSResponse) __envelope.get(j, AXSResponse.class);
                    this.responses.add(j1);

                    continue;
                }

            }

        }



    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.batchStatusCode != null ? this.batchStatusCode : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +1 && propertyIndex < +1 + this.responses.size())
        {
            return this.responses.get(propertyIndex - (+1));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 1 + responses.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "batchStatusCode";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +1 && propertyIndex <= +1 + this.responses.size())
        {
            info.type = AXSResponse.class;
            info.name = "responses";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
