/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;

import org.ksoap2.serialization.*;

import java.util.ArrayList;

import org.ksoap2.serialization.PropertyInfo;

public class AXSOfferAttributeRequirements extends AttributeContainer implements KvmSerializable
{


    public ArrayList<AXSNameValuePairImpl> attributes = new ArrayList<AXSNameValuePairImpl>();

    public ArrayList<AXSOfferAttributeRequirements> childRequirements = new ArrayList<AXSOfferAttributeRequirements>();

    public Integer numberRequested;

    public AXSOfferAttributeRequirements()
    {
    }

    public AXSOfferAttributeRequirements(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue();
                if (info.name.equals("attributes"))
                {

                    if (this.attributes == null)
                    {
                        this.attributes = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.attributes.add(j1);

                    continue;
                }
                if (info.name.equals("childRequirements"))
                {

                    if (this.childRequirements == null)
                    {
                        this.childRequirements = new ArrayList<AXSOfferAttributeRequirements>();
                    }
                    java.lang.Object j = obj;
                    AXSOfferAttributeRequirements j1 = (AXSOfferAttributeRequirements) __envelope.get(j, AXSOfferAttributeRequirements.class);
                    this.childRequirements.add(j1);

                    continue;
                }
                if (info.name.equals("numberRequested"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.numberRequested = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.numberRequested = (Integer) obj;
                    }
                    continue;
                }

            }

        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.numberRequested != null ? this.numberRequested : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +1 && propertyIndex < +1 + this.attributes.size())
        {
            return this.attributes.get(propertyIndex - (+1));
        }
        if (propertyIndex >= +1 + this.attributes.size() && propertyIndex < +1 + this.attributes.size() + this.childRequirements.size())
        {
            return this.childRequirements.get(propertyIndex - (+1 + this.attributes.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 1 + attributes.size() + childRequirements.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "numberRequested";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +1 && propertyIndex <= +1 + this.attributes.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "attributes";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +1 + this.attributes.size() && propertyIndex <= +1 + this.attributes.size() + this.childRequirements.size())
        {
            info.type = AXSOfferAttributeRequirements.class;
            info.name = "childRequirements";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
