/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class AXSOffer extends AttributeContainer implements KvmSerializable
{


    public ArrayList<AXSNameValuePairImpl> additionalAttributes = new ArrayList<AXSNameValuePairImpl>();
    
    public String description;

    public ArrayList<String> offerCode = new ArrayList<String>();
    
    public String offerName;

    public Integer score;

    public String treatmentCode;

    public AXSOffer()
    {
    }

    public AXSOffer(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("additionalAttributes"))
                {

                    if (this.additionalAttributes == null)
                    {
                        this.additionalAttributes = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.additionalAttributes.add(j1);

                    continue;
                }
                if (info.name.equals("description"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.description = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.description = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("offerCode"))
                {

                    if (this.offerCode == null)
                    {
                        this.offerCode = new ArrayList<String>();
                    }
                    java.lang.Object j = obj;
                    String j1 = j.toString();
                    this.offerCode.add(j1);

                    continue;
                }
                if (info.name.equals("offerName"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.offerName = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.offerName = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("score"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.score = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.score = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("treatmentCode"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.treatmentCode = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.treatmentCode = (String) obj;
                    }
                    continue;
                }

            }

        }



    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.description != null ? this.description : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.offerName != null ? this.offerName : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this.score != null ? this.score : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.treatmentCode != null ? this.treatmentCode : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +4 && propertyIndex < +4 + this.additionalAttributes.size())
        {
            return this.additionalAttributes.get(propertyIndex - (+4));
        }
        if (propertyIndex >= +4 + this.additionalAttributes.size() && propertyIndex < +4 + this.additionalAttributes.size() + this.offerCode.size())
        {
            return this.offerCode.get(propertyIndex - (+4 + this.additionalAttributes.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 4 + additionalAttributes.size() + offerCode.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "description";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "offerName";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "score";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "treatmentCode";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +4 && propertyIndex <= +4 + this.additionalAttributes.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "additionalAttributes";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +4 + this.additionalAttributes.size() && propertyIndex <= +4 + this.additionalAttributes.size() + this.offerCode.size())
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "offerCode";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
