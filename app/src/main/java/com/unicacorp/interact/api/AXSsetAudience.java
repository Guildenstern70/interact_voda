/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;

import org.ksoap2.serialization.*;

import java.util.ArrayList;

import org.ksoap2.serialization.PropertyInfo;

public class AXSsetAudience extends AttributeContainer implements KvmSerializable
{


    public String sessionID;

    public ArrayList<AXSNameValuePairImpl> audienceID = new ArrayList<AXSNameValuePairImpl>();

    public String audienceLevel;

    public ArrayList<AXSNameValuePairImpl> parameters = new ArrayList<AXSNameValuePairImpl>();

    public AXSsetAudience()
    {
    }

    public AXSsetAudience(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue();
                if (info.name.equals("sessionID"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.sessionID = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.sessionID = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("audienceID"))
                {

                    if (this.audienceID == null)
                    {
                        this.audienceID = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.audienceID.add(j1);

                    continue;
                }
                if (info.name.equals("audienceLevel"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.audienceLevel = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.audienceLevel = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("parameters"))
                {

                    if (this.parameters == null)
                    {
                        this.parameters = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.parameters.add(j1);

                    continue;
                }

            }

        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {

        if (propertyIndex == 0)
        {
            return this.sessionID != null ? this.sessionID : SoapPrimitive.NullNilElement;
        }

        if (propertyIndex >= 1 && propertyIndex < (1 + this.audienceID.size()))
        {
            return this.audienceID.get(propertyIndex - 1);
        }

        if (propertyIndex == (1 + this.audienceID.size()))
        {
            return this.audienceLevel != null ? this.audienceLevel : SoapPrimitive.NullNilElement;
        }

        if (propertyIndex >= +2 + this.audienceID.size() && propertyIndex < +2 + this.audienceID.size() + this.parameters.size())
        {
            return this.parameters.get(propertyIndex - (+2 + this.audienceID.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 2 + audienceID.size() + parameters.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "sessionID";
            info.namespace = "http://soap.api.interact.unicacorp.com";
        }

        if (propertyIndex >= 1 && propertyIndex < (1 + this.audienceID.size()))
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "audienceID";
            info.namespace = "http://soap.api.interact.unicacorp.com";
        }

        if (propertyIndex == (1 + this.audienceID.size()))
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "audienceLevel";
            info.namespace = "http://soap.api.interact.unicacorp.com";
        }

        if (propertyIndex >= +2 + this.audienceID.size() && propertyIndex <= +2 + this.audienceID.size() + this.parameters.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "parameters";
            info.namespace = "http://soap.api.interact.unicacorp.com";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
