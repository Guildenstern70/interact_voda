/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class AXSAdvisoryMessage extends AttributeContainer implements KvmSerializable
{


    public String detailMessage;

    public String message;

    public Integer messageCode;

    public Integer statusLevel;

    public AXSAdvisoryMessage()
    {
    }

    public AXSAdvisoryMessage(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("detailMessage"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.detailMessage = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.detailMessage = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("message"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.message = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.message = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("messageCode"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.messageCode = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.messageCode = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("statusLevel"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.statusLevel = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.statusLevel = (Integer) obj;
                    }
                    continue;
                }

            }

        }



    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.detailMessage != null ? this.detailMessage : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.message != null ? this.message : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this.messageCode != null ? this.messageCode : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.statusLevel != null ? this.statusLevel : SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 4;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "detailMessage";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "message";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "messageCode";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "statusLevel";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
