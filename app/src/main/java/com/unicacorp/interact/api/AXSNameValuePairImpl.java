/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class AXSNameValuePairImpl extends AttributeContainer implements KvmSerializable
{


    public String name;

    public java.util.Date valueAsDate;

    public Double valueAsNumeric;

    public String valueAsString;

    public String valueDataType;

    public AXSNameValuePairImpl()
    {
    }

    public AXSNameValuePairImpl(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("name"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.name = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.name = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("valueAsDate"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.valueAsDate = AXSHelper.ConvertFromWebService(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof java.util.Date)
                    {
                        this.valueAsDate = (java.util.Date) obj;
                    }
                    continue;
                }
                if (info.name.equals("valueAsNumeric"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.valueAsNumeric = new Double(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Double)
                    {
                        this.valueAsNumeric = (Double) obj;
                    }
                    continue;
                }
                if (info.name.equals("valueAsString"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.valueAsString = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.valueAsString = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("valueDataType"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.valueDataType = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.valueDataType = (String) obj;
                    }
                    continue;
                }

            }

        }



    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.name != null ? this.name : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.valueAsDate != null ? AXSHelper.getDateFormat().format(this.valueAsDate) : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this.valueAsNumeric != null ? this.valueAsNumeric : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.valueAsString != null ? this.valueAsString : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 4)
        {
            return this.valueDataType != null ? this.valueDataType : SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 5;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "name";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "valueAsDate";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = Double.class;
            info.name = "valueAsNumeric";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "valueAsString";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 4)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "valueDataType";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
