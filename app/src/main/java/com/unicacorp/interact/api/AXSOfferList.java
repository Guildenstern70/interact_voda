/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;
import java.util.ArrayList;
import org.ksoap2.serialization.PropertyInfo;

public class AXSOfferList extends AttributeContainer implements KvmSerializable
{


    public String defaultString;

    public String interactionPointName;

    public ArrayList<AXSOffer> recommendedOffers = new ArrayList<AXSOffer>();

    public AXSOfferList()
    {
    }

    public AXSOfferList(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("defaultString"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.defaultString = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.defaultString = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("interactionPointName"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.interactionPointName = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.interactionPointName = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("recommendedOffers"))
                {

                    if (this.recommendedOffers == null)
                    {
                        this.recommendedOffers = new ArrayList<AXSOffer>();
                    }
                    java.lang.Object j = obj;
                    AXSOffer j1 = (AXSOffer) __envelope.get(j, AXSOffer.class);
                    this.recommendedOffers.add(j1);

                    continue;
                }

            }

        }



    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.defaultString != null ? this.defaultString : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.interactionPointName != null ? this.interactionPointName : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +2 && propertyIndex < +2 + this.recommendedOffers.size())
        {
            return this.recommendedOffers.get(propertyIndex - (+2));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 2 + recommendedOffers.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "defaultString";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "interactionPointName";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +2 && propertyIndex <= +2 + this.recommendedOffers.size())
        {
            info.type = AXSOffer.class;
            info.name = "recommendedOffers";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
