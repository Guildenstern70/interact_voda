/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;


// Interact XML/POJO Mapping


import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.*;
import org.ksoap2.transport.*;
import org.kxml2.kdom.Element;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AXSInteractSoapServiceSoap12Binding
{
    interface AXSIWcfMethod
    {
        AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope() throws java.lang.Exception;

        java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object result) throws java.lang.Exception;
    }

    String url = "http://185.64.246.229:7001/interact/services/InteractService";

    int timeOut = 60000;
    public List<HeaderProperty> httpHeaders;
    public boolean enableLogging;

    AXSIServiceEvents callback;

    public AXSInteractSoapServiceSoap12Binding()
    {
    }

    public AXSInteractSoapServiceSoap12Binding(AXSIServiceEvents callback)
    {
        this.callback = callback;
    }

    public AXSInteractSoapServiceSoap12Binding(AXSIServiceEvents callback, String url)
    {
        this.callback = callback;
        this.url = url;
    }

    public AXSInteractSoapServiceSoap12Binding(AXSIServiceEvents callback, String url, int timeOut)
    {
        this.callback = callback;
        this.url = url;
        this.timeOut = timeOut;
    }

    protected org.ksoap2.transport.Transport createTransport()
    {
        try
        {
            java.net.URI uri = new java.net.URI(url);
            if (uri.getScheme().equalsIgnoreCase("https"))
            {
                int port = uri.getPort() > 0 ? uri.getPort() : 443;
                return new HttpsTransportSE(uri.getHost(), port, uri.getPath(), timeOut);
            }
            else
            {
                return new HttpTransportSE(url, timeOut);
            }

        }
        catch (java.net.URISyntaxException e)
        {
        }
        return null;
    }

    protected AXSExtendedSoapSerializationEnvelope createEnvelope()
    {
        AXSExtendedSoapSerializationEnvelope envelope = new AXSExtendedSoapSerializationEnvelope(AXSExtendedSoapSerializationEnvelope.VER12);
        return envelope;
    }

    protected java.util.List sendRequest(String methodName, AXSExtendedSoapSerializationEnvelope envelope, org.ksoap2.transport.Transport transport) throws java.lang.Exception
    {
        return transport.call(methodName, envelope, httpHeaders);
    }

    java.lang.Object getResult(java.lang.Class destObj, java.lang.Object source, String resultName, AXSExtendedSoapSerializationEnvelope __envelope) throws java.lang.Exception
    {
        if (source == null)
        {
            return null;
        }
        if (source instanceof SoapPrimitive)
        {
            SoapPrimitive soap = (SoapPrimitive) source;
            if (soap.getName().equals(resultName))
            {
                java.lang.Object instance = __envelope.get(source, destObj);
                return instance;
            }
        }
        else
        {
            SoapObject soap = (SoapObject) source;
            if (soap.hasProperty(resultName))
            {
                java.lang.Object j = soap.getProperty(resultName);
                if (j == null)
                {
                    return null;
                }
                java.lang.Object instance = __envelope.get(j, destObj);
                return instance;
            }
            else if (soap.getName().equals(resultName))
            {
                java.lang.Object instance = __envelope.get(source, destObj);
                return instance;
            }
        }

        return null;
    }


    public AXSResponse getOffersForMultipleInteractionPoints(final AXSgetOffersForMultipleInteractionPoints getOffersForMultipleInteractionPoints) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                __envelope.addMapping("http://soap.api.interact.unicacorp.com", "getOffersForMultipleInteractionPoints", new AXSgetOffersForMultipleInteractionPoints().getClass());
                __envelope.setOutputSoapObject(getOffersForMultipleInteractionPoints);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:getOffersForMultipleInteractionPoints");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> getOffersForMultipleInteractionPointsAsync(final AXSgetOffersForMultipleInteractionPoints getOffersForMultipleInteractionPoints)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return getOffersForMultipleInteractionPoints(getOffersForMultipleInteractionPoints);
            }
        });
    }

    public AXSResponse getProfile(final String sessionID) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://soap.api.interact.unicacorp.com", "getProfile");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "sessionID";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(sessionID != null ? sessionID : SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:getProfile");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> getProfileAsync(final String sessionID)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return getProfile(sessionID);
            }
        });
    }

    public AXSResponse getVersion() throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:getVersion");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> getVersionAsync()
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return getVersion();
            }
        });
    }

    public AXSResponse postEvent(final AXSpostEvent postEvent) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                __envelope.addMapping("http://soap.api.interact.unicacorp.com", "postEvent", new AXSpostEvent().getClass());
                __envelope.setOutputSoapObject(postEvent);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:postEvent");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> postEventAsync(final AXSpostEvent postEvent)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return postEvent(postEvent);
            }
        });
    }

    public AXSBatchResponse executeBatch(final AXSexecuteBatch executeBatch) throws java.lang.Exception
    {
        return (AXSBatchResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                __envelope.addMapping("http://soap.api.interact.unicacorp.com", "executeBatch", new AXSexecuteBatch().getClass());
                __envelope.setOutputSoapObject(executeBatch);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSBatchResponse) getResult(AXSBatchResponse.class, __result, "return", __envelope);
            }
        }, "urn:executeBatch");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSBatchResponse>> executeBatchAsync(final AXSexecuteBatch executeBatch)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSBatchResponse>()
        {
            public AXSBatchResponse Func() throws java.lang.Exception
            {
                return executeBatch(executeBatch);
            }
        });
    }

    public AXSResponse getOffers(final String sessionID, final String iPoint, final Integer numberRequested) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://soap.api.interact.unicacorp.com", "getOffers");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "sessionID";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(sessionID != null ? sessionID : SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "iPoint";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(iPoint != null ? iPoint : SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "numberRequested";
                __info.type = PropertyInfo.INTEGER_CLASS;
                __info.setValue(numberRequested);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:getOffers");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> getOffersAsync(final String sessionID, final String iPoint, final Integer numberRequested)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return getOffers(sessionID, iPoint, numberRequested);
            }
        });
    }

    public AXSResponse setDebug(final String sessionID, final Boolean debug) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://soap.api.interact.unicacorp.com", "setDebug");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "sessionID";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(sessionID != null ? sessionID : SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "debug";
                __info.type = PropertyInfo.BOOLEAN_CLASS;
                __info.setValue(debug);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:setDebug");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> setDebugAsync(final String sessionID, final Boolean debug)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return setDebug(sessionID, debug);
            }
        });
    }

    public AXSResponse setAudience(final AXSsetAudience setAudience) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                __envelope.addMapping("http://soap.api.interact.unicacorp.com", "setAudience", new AXSsetAudience().getClass());
                __envelope.setOutputSoapObject(setAudience);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:setAudience");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> setAudienceAsync(final AXSsetAudience setAudience)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return setAudience(setAudience);
            }
        });
    }

    public AXSResponse startSession(final AXSstartSession startSession) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                __envelope.addMapping("http://soap.api.interact.unicacorp.com", "startSession", new AXSstartSession().getClass());
                __envelope.setOutputSoapObject(startSession);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:startSession");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> startSessionAsync(final AXSstartSession startSession)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return startSession(startSession);
            }
        });
    }

    public AXSResponse endSession(final String sessionID) throws java.lang.Exception
    {
        return (AXSResponse) execute(new AXSIWcfMethod()
        {
            @Override
            public AXSExtendedSoapSerializationEnvelope CreateSoapEnvelope()
            {
                AXSExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://soap.api.interact.unicacorp.com", "endSession");
                __envelope.setOutputSoapObject(__soapReq);

                PropertyInfo __info = null;
                __info = new PropertyInfo();
                __info.namespace = "http://soap.api.interact.unicacorp.com";
                __info.name = "sessionID";
                __info.type = PropertyInfo.STRING_CLASS;
                __info.setValue(sessionID != null ? sessionID : SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                return __envelope;
            }

            @Override
            public java.lang.Object ProcessResult(AXSExtendedSoapSerializationEnvelope __envelope, java.lang.Object __result) throws java.lang.Exception
            {
                return (AXSResponse) getResult(AXSResponse.class, __result, "return", __envelope);
            }
        }, "urn:endSession");
    }

    public android.os.AsyncTask<Void, Void, AXSOperationResult<AXSResponse>> endSessionAsync(final String sessionID)
    {
        return executeAsync(new AXSFunctions.IFunc<AXSResponse>()
        {
            public AXSResponse Func() throws java.lang.Exception
            {
                return endSession(sessionID);
            }
        });
    }


    protected java.lang.Object execute(AXSIWcfMethod wcfMethod, String methodName) throws java.lang.Exception
    {
        org.ksoap2.transport.Transport __httpTransport = createTransport();
        __httpTransport.debug = enableLogging;
        AXSExtendedSoapSerializationEnvelope __envelope = wcfMethod.CreateSoapEnvelope();
        try
        {
            sendRequest(methodName, __envelope, __httpTransport);

        }
        finally
        {
            if (__httpTransport.debug)
            {
                if (__httpTransport.requestDump != null)
                {
                    android.util.Log.i("requestDump", __httpTransport.requestDump);
                }
                if (__httpTransport.responseDump != null)
                {
                    android.util.Log.i("responseDump", __httpTransport.responseDump);
                }
            }
        }
        java.lang.Object __retObj = __envelope.bodyIn;
        if (__retObj instanceof org.ksoap2.SoapFault)
        {
            org.ksoap2.SoapFault __fault = (org.ksoap2.SoapFault) __retObj;
            throw convertToException(__fault, __envelope);
        }
        else
        {
            return wcfMethod.ProcessResult(__envelope, __retObj);
        }
    }

    protected <T> android.os.AsyncTask<Void, Void, AXSOperationResult<T>> executeAsync(final AXSFunctions.IFunc<T> func)
    {
        return new android.os.AsyncTask<Void, Void, AXSOperationResult<T>>()
        {
            @Override
            protected void onPreExecute()
            {
                callback.Starting();
            }

            ;

            @Override
            protected AXSOperationResult<T> doInBackground(Void... params)
            {
                AXSOperationResult<T> result = new AXSOperationResult<T>();
                try
                {
                    result.Result = func.Func();
                }
                catch (java.lang.Exception ex)
                {
                    ex.printStackTrace();
                    result.Exception = ex;
                }
                return result;
            }

            @Override
            protected void onPostExecute(AXSOperationResult<T> result)
            {
                callback.Completed(result);
            }
        }.execute();
    }

    java.lang.Exception convertToException(org.ksoap2.SoapFault fault, AXSExtendedSoapSerializationEnvelope envelope)
    {

        return new java.lang.Exception(fault.faultstring);
    }
}


