/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;
import org.ksoap2.serialization.*;

public class AXSGetOfferRequest extends AttributeContainer implements KvmSerializable
{


    public Integer duplicationPolicy;

    public String ipName;

    public Integer numberRequested;

    public AXSOfferAttributeRequirements offerAttributes;

    public AXSGetOfferRequest()
    {
    }

    public AXSGetOfferRequest(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue(); 
                if (info.name.equals("duplicationPolicy"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.duplicationPolicy = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.duplicationPolicy = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("ipName"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.ipName = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.ipName = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("numberRequested"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.numberRequested = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.numberRequested = (Integer) obj;
                    }
                    continue;
                }
                if (info.name.equals("offerAttributes"))
                {
                    java.lang.Object j = obj;
                    this.offerAttributes = (AXSOfferAttributeRequirements) __envelope.get(j, AXSOfferAttributeRequirements.class);
                    continue;
                }

            }

        }

    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.duplicationPolicy != null ? this.duplicationPolicy : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.ipName != null ? this.ipName : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this.numberRequested != null ? this.numberRequested : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.offerAttributes != null ? this.offerAttributes : SoapPrimitive.NullSkip;
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 4;
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "duplicationPolicy";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "ipName";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "numberRequested";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = AXSOfferAttributeRequirements.class;
            info.name = "offerAttributes";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
