/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

/**
 *
 */
public class InteractSoapWrapper extends AXSInteractSoapServiceSoap11Binding
{
    /**
     * @param interactServerIP   ie:185.64.246.229
     * @param interactServerPort ie:7001
     */
    public InteractSoapWrapper(String interactServerIP, String interactServerPort)
    {
        StringBuilder urlBuilder = new StringBuilder("http://");
        urlBuilder.append(interactServerIP);
        urlBuilder.append(":");
        urlBuilder.append(interactServerPort);
        urlBuilder.append("/interact/services/InteractService");
        this.url = urlBuilder.toString();
    }

    /**
     * @param interactServerIPandPort ie:185.64.246.229:7001
     */
    public InteractSoapWrapper(String interactServerIPandPort)
    {
        StringBuilder urlBuilder = new StringBuilder("http://");
        urlBuilder.append(interactServerIPandPort);
        urlBuilder.append("/interact/services/InteractService");
        this.url = urlBuilder.toString();
    }
}
