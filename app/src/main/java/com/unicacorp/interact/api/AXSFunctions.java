/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


public class AXSFunctions
{
    public interface IFunc<Res>
    {
        Res Func() throws java.lang.Exception;
    }

    public interface IFunc1<T, Res>
    {
        Res Func(T p);
    }

    public interface IFunc2<T1, T2, Res>
    {
        Res Func(T1 p1, T2 p2);
    }

    public interface IAction
    {
        void Action();
    }

    public interface IAction1<T>
    {
        void Action(T p1);
    }
}

