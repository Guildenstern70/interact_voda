/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package com.unicacorp.interact.api;

// Interact XML/POJO Mapping


import java.util.Hashtable;

import org.ksoap2.serialization.*;

import java.util.ArrayList;

import org.ksoap2.serialization.PropertyInfo;

public class AXSResponse extends AttributeContainer implements KvmSerializable
{


    public ArrayList<AXSAdvisoryMessage> advisoryMessages = new ArrayList<AXSAdvisoryMessage>();

    public ArrayList<AXSOfferList> allOfferLists = new ArrayList<AXSOfferList>();

    public String apiVersion;

    public AXSOfferList offerList;

    public ArrayList<AXSNameValuePairImpl> profileRecord = new ArrayList<AXSNameValuePairImpl>();

    public String sessionID;

    public Integer statusCode;

    public AXSResponse()
    {
    }

    public AXSResponse(java.lang.Object paramObj, AXSExtendedSoapSerializationEnvelope __envelope)
    {

        if (paramObj == null)
            return;
        AttributeContainer inObj = (AttributeContainer) paramObj;


        if (inObj instanceof SoapObject)
        {
            SoapObject soapObject = (SoapObject) inObj;
            int size = soapObject.getPropertyCount();
            for (int i0 = 0; i0 < size; i0++)
            {
                //if you have compilation error here, please use a ksoap2.jar and ExKsoap2.jar from libs folder (in the generated zip file)
                PropertyInfo info = soapObject.getPropertyInfo(i0);
                java.lang.Object obj = info.getValue();
                if (info.name.equals("advisoryMessages"))
                {

                    if (this.advisoryMessages == null)
                    {
                        this.advisoryMessages = new ArrayList<AXSAdvisoryMessage>();
                    }
                    java.lang.Object j = obj;
                    AXSAdvisoryMessage j1 = (AXSAdvisoryMessage) __envelope.get(j, AXSAdvisoryMessage.class);
                    this.advisoryMessages.add(j1);

                    continue;
                }
                if (info.name.equals("allOfferLists"))
                {

                    if (this.allOfferLists == null)
                    {
                        this.allOfferLists = new ArrayList<AXSOfferList>();
                    }
                    java.lang.Object j = obj;
                    AXSOfferList j1 = (AXSOfferList) __envelope.get(j, AXSOfferList.class);
                    this.allOfferLists.add(j1);

                    continue;
                }
                if (info.name.equals("apiVersion"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.apiVersion = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.apiVersion = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("offerList"))
                {
                    java.lang.Object j = obj;
                    this.offerList = (AXSOfferList) __envelope.get(j, AXSOfferList.class);
                    continue;
                }
                if (info.name.equals("profileRecord"))
                {

                    if (this.profileRecord == null)
                    {
                        this.profileRecord = new ArrayList<AXSNameValuePairImpl>();
                    }
                    java.lang.Object j = obj;
                    AXSNameValuePairImpl j1 = (AXSNameValuePairImpl) __envelope.get(j, AXSNameValuePairImpl.class);
                    this.profileRecord.add(j1);

                    continue;
                }
                if (info.name.equals("sessionID"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.sessionID = j.toString();
                        }
                    }
                    else if (obj != null && obj instanceof String)
                    {
                        this.sessionID = (String) obj;
                    }
                    continue;
                }
                if (info.name.equals("statusCode"))
                {

                    if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                    {
                        SoapPrimitive j = (SoapPrimitive) obj;
                        if (j.toString() != null)
                        {
                            this.statusCode = Integer.parseInt(j.toString());
                        }
                    }
                    else if (obj != null && obj instanceof Integer)
                    {
                        this.statusCode = (Integer) obj;
                    }
                    continue;
                }

            }

        }


    }

    @Override
    public java.lang.Object getProperty(int propertyIndex)
    {
        //!!!!! If you have a compilation error here then you are using old version of ksoap2 library. Please upgrade to the latest version.
        //!!!!! You can find a correct version in Lib folder from generated zip file!!!!!
        if (propertyIndex == 0)
        {
            return this.apiVersion != null ? this.apiVersion : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 1)
        {
            return this.offerList != null ? this.offerList : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 2)
        {
            return this.sessionID != null ? this.sessionID : SoapPrimitive.NullSkip;
        }
        if (propertyIndex == 3)
        {
            return this.statusCode != null ? this.statusCode : SoapPrimitive.NullSkip;
        }
        if (propertyIndex >= +4 && propertyIndex < +4 + this.advisoryMessages.size())
        {
            return this.advisoryMessages.get(propertyIndex - (+4));
        }
        if (propertyIndex >= +4 + this.advisoryMessages.size() && propertyIndex < +4 + this.advisoryMessages.size() + this.allOfferLists.size())
        {
            return this.allOfferLists.get(propertyIndex - (+4 + this.advisoryMessages.size()));
        }
        if (propertyIndex >= +4 + this.advisoryMessages.size() + this.allOfferLists.size() && propertyIndex < +4 + this.advisoryMessages.size() + this.allOfferLists.size() + this.profileRecord.size())
        {
            return this.profileRecord.get(propertyIndex - (+4 + this.advisoryMessages.size() + this.allOfferLists.size()));
        }
        return null;
    }


    @Override
    public int getPropertyCount()
    {
        return 4 + advisoryMessages.size() + allOfferLists.size() + profileRecord.size();
    }

    @Override
    public void getPropertyInfo(int propertyIndex, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info)
    {
        if (propertyIndex == 0)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "apiVersion";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 1)
        {
            info.type = AXSOfferList.class;
            info.name = "offerList";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 2)
        {
            info.type = PropertyInfo.STRING_CLASS;
            info.name = "sessionID";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex == 3)
        {
            info.type = PropertyInfo.INTEGER_CLASS;
            info.name = "statusCode";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +4 && propertyIndex <= +4 + this.advisoryMessages.size())
        {
            info.type = AXSAdvisoryMessage.class;
            info.name = "advisoryMessages";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +4 + this.advisoryMessages.size() && propertyIndex <= +4 + this.advisoryMessages.size() + this.allOfferLists.size())
        {
            info.type = AXSOfferList.class;
            info.name = "allOfferLists";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
        if (propertyIndex >= +4 + this.advisoryMessages.size() + this.allOfferLists.size() && propertyIndex <= +4 + this.advisoryMessages.size() + this.allOfferLists.size() + this.profileRecord.size())
        {
            info.type = AXSNameValuePairImpl.class;
            info.name = "profileRecord";
            info.namespace = "http://api.interact.unicacorp.com/xsd";
        }
    }

    @Override
    public void setProperty(int arg0, java.lang.Object arg1)
    {
    }

}
