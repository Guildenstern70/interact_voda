/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.config;

public class SetAudienceConfig
{
    public final static String FLOWCHART_NAME = "SEG_INDIVIDUAL";
    public final static String AUDIENCE_LEVEL = "Individual";
    public final static String AUDIENCE_ID_KEY = "indiv_id";
}

