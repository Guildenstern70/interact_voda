/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import com.unicacorp.interact.api.AXSNameValuePairImpl;
import com.unicacorp.interact.api.AXSOffer;
import com.unicacorp.interact.api.AXSOfferList;
import com.unicacorp.interact.api.AXSResponse;

import java.util.ArrayList;

/**
 *
 */
public class BaseInteractTest
{
    protected Session session;

    protected void log(String message)
    {
        System.out.println(message);
    }

}
