/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc;

import com.unicacorp.interact.api.AXSOfferList;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import vodafone.ibm.com.it.vodafonepoc.config.GetOffersConfig;
import vodafone.ibm.com.it.vodafonepoc.config.PostEventConfig;
import vodafone.ibm.com.it.vodafonepoc.config.SetAudienceConfig;
import vodafone.ibm.com.it.vodafonepoc.config.StartSessionConfig;
import vodafone.ibm.com.it.vodafonepoc.tasks.GetOffersTask;
import vodafone.ibm.com.it.vodafonepoc.tasks.GetProfileTask;
import vodafone.ibm.com.it.vodafonepoc.tasks.PostEventTask;
import vodafone.ibm.com.it.vodafonepoc.tasks.SetAudienceTask;
import vodafone.ibm.com.it.vodafonepoc.tasks.StartSessionTask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 */
public class InteractClientTest extends BaseInteractTest
{

    @Before
    public void setUp()
    {
        this.session = Session.getInstance(null);
        this.log("Session #" + this.session.getSessionId());
    }

    @Test
    public void getFirstOfferTest() throws Exception
    {
        this.startSession();
        double indiv_id = this.getProfile();
        this.setAudience(indiv_id);
        this.getOffers(GetOffersConfig.INTERACTION_POINT_1);
    }

    @Test
    public void getSecondOfferTest() throws Exception
    {
        this.startSession();
        double indiv_id = this.getProfile();
        this.setAudience(indiv_id);
        this.getOffers(GetOffersConfig.INTERACTION_POINT_2);  // Get Counters

        this.postEvent();  // Set counters
        this.getOffers(GetOffersConfig.INTERACTION_POINT_1);  // Get Banner ID
    }

    private void startSession() throws Exception
    {
        this.log("*** TEST START SESSION [BEGIN] ***");

        Audience audience = new Audience();
        audience.setFlowchartName(StartSessionConfig.FLOWCHART_NAME);
        audience.setAudienceIdKey(StartSessionConfig.AUDIENCE_ID_KEY);
        audience.setAudienceIdValue(StartSessionConfig.AUDIENCE_ID_VALUE);
        audience.setAudienceLevel(StartSessionConfig.AUDIENCE_LEVEL);

        this.log(audience.toString());

        StartSessionTask task = new StartSessionTask(this.session, audience, StartSessionConfig.INTERACTIVE_CHANNEL);

        this.log(" =Input parameters:");
        this.log(task.getInputDetails());
        String response = task.perform();
        assertNotNull(response);
        this.log(" =Output response:");
        this.log(response);
        assertEquals(0, task.getStatusCode());
        this.log("*** TEST START SESSION [END] ***");
    }

    private double getProfile() throws Exception
    {
        this.log("*** TEST GET PROFILE [BEGIN] ***");
        GetProfileTask task = new GetProfileTask(this.session);
        String response = task.perform();
        assertNotNull(response);
        this.log(" =Output response:");
        double indiv_id = task.getIndividual_id();
        this.log("  Indiv_ID = " + String.valueOf(indiv_id));
        this.log(response);
        assertEquals(0, task.getStatusCode());
        this.log("*** TEST GET PROFILE [END] ***");
        return indiv_id;
    }

    private void setAudience(double indiv_id) throws Exception
    {
        this.log("*** TEST SET AUDIENCE [BEGIN] ***");

        Audience audience = new Audience();
        audience.setFlowchartName(SetAudienceConfig.FLOWCHART_NAME);
        audience.setAudienceIdKey(SetAudienceConfig.AUDIENCE_ID_KEY);
        audience.setAudienceIdValue(String.valueOf(indiv_id));
        audience.setAudienceLevel(SetAudienceConfig.AUDIENCE_LEVEL);

        this.log(audience.toString());

        SetAudienceTask task = new SetAudienceTask(this.session, audience);
        String response = task.perform();
        assertNotNull(response);
        this.log(" =Output response:");
        this.log(response);
        assertEquals(0, task.getStatusCode());
        this.log("*** TEST SET AUDIENCE [END] ***");
    }

    private void getOffers(String interactionPoint) throws Exception
    {
        this.log("*** TEST GET OFFERS [BEGIN] ***");
        GetOffersTask task = new GetOffersTask(this.session,
                interactionPoint,
                GetOffersConfig.NUMBER_REQUESTED);
        this.log(" =Input parameters:");
        this.log(task.getInputDetails());
        String response = task.perform();
        ArrayList<AXSOfferList> offers = task.getOffersList();
        String bannerId = task.getBannerID();
        if (bannerId != null)
        {
            this.log(" FOUND Banner ID:");
            this.log(task.getBannerID());
        }
        Counters counters = task.getCounters();
        if (counters != null)
        {
            this.log(" FOUND Counters:");
            this.log(counters.toString());
        }

        this.log(" =Output response:");
        this.log(InteractProxy.processOffersList(offers));
        assertNotNull(response);
        this.log(response);
        assertEquals(0, task.getStatusCode());
        this.log("*** TEST GET OFFERS [END] ***");
    }

    private void postEvent() throws Exception
    {
        this.log("*** TEST POST EVENT [BEGIN] ***");

        Counters counters = new Counters();
        counters.setDati("1.7GB");
        counters.setVoce("200min");
        counters.setSms("150");

        PostEventTask task = new PostEventTask(this.session,
                PostEventConfig.FLOWCHART_NAME,
                PostEventConfig.EVENT_NAME, counters);

        this.log(" =Input parameters:");
        this.log(task.getInputDetails());

        String response = task.perform();

        this.log(" =Output response:");
        assertNotNull(response);
        this.log(response);
        assertEquals(0, task.getStatusCode());

        this.log("*** TEST POST EVENT [END] ***");

    }

}
