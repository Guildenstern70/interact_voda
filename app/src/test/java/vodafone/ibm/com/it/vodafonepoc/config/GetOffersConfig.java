/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.config;

/**
 *
 */
public class GetOffersConfig
{
    public final static String INTERACTION_POINT_1 = "HOME";
    public final static String INTERACTION_POINT_2 = "CONTATORE";
    public final static int NUMBER_REQUESTED = 1;
}
