/*
 *
 *  * Licensed Materials - Property of IBM
 *  *
 *  * 5725E28, 5725I03
 *  *
 *  * © Copyright IBM Corp. 2011, 2016
 *  *
 *  * US Government Users Restricted Rights - Use, duplication or disclosure restricted by
 *  * GSA ADP Schedule Contract with IBM Corp.
 *  *
 *
 */

package vodafone.ibm.com.it.vodafonepoc.config;

/**
 *
 */
public class PostEventConfig
{
    public final static String FLOWCHART_NAME = "SEG_INDIVIDUAL";
    public final static String INTERACTIVE_CHANNEL = "VODAFONE";
    public final static String EVENT_NAME = "check_contatori";
}
